# Purpose

The purpose of this project is to demo a REST API using Spring Boot and Java.

The API should be simple, having only 1 endpoint under `/world`

The expected result should be:
- When requesting with the verb Get to the `http://localhost:8080/world` URL, the response should be:

``` JSON
{
    "id": <Random Signed Integer>,
    "message": "Hello World"
}
```

# Tests

The tests currently either call the function directly, for Unit Tests, or run a lightweight version of the Framework to make Requests and run Integration Tests.

The tests will run some basic checks against the expected datatype, and will discard anything that has different data structure as described above.

# CI/CD - Gitlab

The pipeline will run the tests on every commit pushed to the Repo. 

Once the Tests have passed, the App will get `Built` and `Packaged` into a Docker Image and uploaded to the Docker Registry available on the project Repo.

In order to download the image you need to run:

```
docker pull registry.gitlab.com/misc17/spring-rest-api-demo-hello-world:latest
```

# Docker

The Docker image is built using the Alpine Distro, with the closest available JDK that Spring can run without any further adjustments.
