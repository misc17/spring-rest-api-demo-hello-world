package demo.raulmoga.springrestapi.helloworld.controllers;

import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import net.minidev.json.JSONObject;

@RestController
public class ApiController {

	/**
	 * Returns a JSONObject with the format {"id": <random int>, "message": "Hello World"}. Only responds to GET method.
	 * 
	 * @return JSONObject
	 */
	@GetMapping(path = "/world",  produces = "application/json")
	public JSONObject getWorld() {
		// Generate Response Object
		JSONObject obj = new JSONObject();
		// Add ID field with random Integer
		obj.appendField("id", new Random().nextInt());
		// Add Message field with "Hello World"
		obj.appendField("message", "Hello World");

		// Return Object, which will get jsonified
		return obj;
	}

}
