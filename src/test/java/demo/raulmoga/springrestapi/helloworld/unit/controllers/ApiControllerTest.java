package demo.raulmoga.springrestapi.helloworld.unit.controllers;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.RegularExpressionValueMatcher;
import org.skyscreamer.jsonassert.comparator.CustomComparator;

import demo.raulmoga.springrestapi.helloworld.controllers.ApiController;
import net.minidev.json.JSONObject;

import org.json.JSONException;

class ApiControllerTest {

    /**
     * Unit Test to check if the data returned has the correct format.
     * Expected JSON format:
     * {
     *  "id": <Integer>,
     *  "message": "Hello World"
     * }
     * 
     * Spring MVC is NOT getting loaded during this test.
     * @throws IllegalArgumentException
     * @throws JSONException
     */
    @Test
    void testGetWorld() throws IllegalArgumentException, JSONException {
        /* Arrange */
        ApiController controller = new ApiController();

        /* Act */
        JSONObject result = controller.getWorld();

        /* Assert */
        /* Expect a response of:
         { 
             "id": <Integer>
             "message": "Hello World"
         }
        */
        JSONAssert.assertEquals(
            // "x" will get replaced with the RegEx equivalent of "(\d)+"    [ One or more numbers ]
            "{id:x, message: \"Hello World\"}",
            result.toString(),

            new CustomComparator(
                JSONCompareMode.STRICT,         // Not allowing this JSON to have more fields than the ones specified
                new Customization("id", new RegularExpressionValueMatcher<Object>("-?(\\d)+"))        // RegEx for the ID
            )
        );
    }

}
