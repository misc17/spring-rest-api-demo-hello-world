package demo.raulmoga.springrestapi.helloworld.integration.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.RegularExpressionValueMatcher;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import demo.raulmoga.springrestapi.helloworld.controllers.ApiController;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ApiController.class)
class ApiControllerTest {
    
    @Autowired
    private MockMvc mvc;

    /**
     * Integration Test to check if the data returned by the "/hello" Endpoint has the correct format.
     * Expected JSON format:
     * {
     *  "id": <Integer>,
     *  "message": "Hello World"
     * }
     * 
     * Spring MVC does get loaded during this test.
     * @throws IllegalArgumentException
     * @throws JSONException
     */
    @Test
    void testGetWorld() throws Exception {
        /* Arrange */
        RequestBuilder request = MockMvcRequestBuilders.get("/world");

        /* Act */
        MvcResult result = mvc.perform(request).andReturn();

        /* Assert */
        /* Expect a response of:
         { 
             "id": <Integer>
             "message": "Hello World"
         }
        */
        JSONAssert.assertEquals(
            // "x" will get replaced with the RegEx equivalent of "(\d)+" [ One or more numbers ]
            "{id:x, message: \"Hello World\"}",
            result.getResponse().getContentAsString(),

            new CustomComparator(
                JSONCompareMode.STRICT,         // Not allowing this JSON to have more fields than the ones specified
                new Customization("id", new RegularExpressionValueMatcher<Object>("-?(\\d)+"))        // RegEx for the ID
            )
        );
    }

}
